docs_gitlab_com_dir = ${gitlab_development_root}/docs-gitlab-com

make_docs_gitlab_com = $(Q)make -C ${docs_gitlab_com_dir}

ifeq ($(docs_gitlab_com_enabled),true)
docs-gitlab-com-setup: docs-gitlab-com/.git docs-gitlab-com-deps docs-gitlab-com-migrate gitlab-docs-yarn-build
else
docs-gitlab-com-setup:
	@true
endif

docs-gitlab-com/.git:
	$(Q)support/component-git-clone ${git_params} ${docs_gitlab_com_repo} docs-gitlab-com

docs-gitlab-com/.git/pull: docs-gitlab-com/.git
	@echo
	@echo "${DIVIDER}"
	@echo "Updating gitlab-org/docs-gitlab-com"
	@echo "${DIVIDER}"
	$(Q)support/component-git-update docs_gitlab_com "${docs_gitlab_com_dir}" main main

.PHONY: docs-gitlab-com-deps
docs-gitlab-com-deps: docs-gitlab-com-asdf-mise-install docs-gitlab-com-yarn

docs-gitlab-com-asdf-mise-install:
	@echo
	@echo "${DIVIDER}"
	@echo "Installing tools from ${docs_gitlab_com_dir}/.tool-versions"
	@echo "${DIVIDER}"
	$(Q)cd ${docs_gitlab_com_dir} && ASDF_DEFAULT_TOOL_VERSIONS_FILENAME="${docs_gitlab_com_dir}/.tool-versions" make install-dependencies

docs-gitlab-com-yarn:
	$(Q)cd ${docs_gitlab_com_dir} && corepack enable && make install-nodejs-dependencies

ifeq ($(docs_gitlab_com_run_migration_scripts),true)
docs-gitlab-com-migrate:
	@echo
	@echo "${DIVIDER}"
	@echo "Running GitLab Docs Hugo migration scripts"
	@echo "${DIVIDER}"
	$(make_docs_gitlab_com) clone-docs-projects
else ifeq ($(docs_gitlab_com_enabled),true)
docs-gitlab-com-migrate:
	@echo
	@echo "${DIVIDER}"
	@echo "Fetching files required by GitLab Docs Hugo"
	@echo "${DIVIDER}"
	$(Q)cd ${docs_gitlab_com_dir} && go run cmd/gldocs/main.go fetch
else
docs-gitlab-com-migrate:
	@true
endif

ifeq ($(docs_gitlab_com_enabled),true)
gitlab-docs-yarn-build:
	@echo
	@echo "${DIVIDER}"
	@echo "Running vite"
	@echo "${DIVIDER}"
	$(Q)cd ${docs_gitlab_com_dir} && yarn build
else
gitlab-docs-yarn-build:
	@true
endif

.PHONY: docs-gitlab-com-update
ifeq ($(docs_gitlab_com_enabled),true)
docs-gitlab-com-update: docs-gitlab-com-update-timed
else
docs-gitlab-com-update:
	@true
endif

.PHONY: docs-gitlab-com-update-run
docs-gitlab-com-update-run: docs-gitlab-com/.git/pull docs-gitlab-com-deps docs-gitlab-com-migrate gitlab-docs-yarn-build
