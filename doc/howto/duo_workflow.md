# Duo Workflow

## Prerequisites

1. Install the [`gcloud` CLI](https://cloud.google.com/sdk/docs/install) and configure it with a suitable project:

   ```shell
   gcloud auth application-default login
   ```

1. Configure a [loopback interface](local_network.md#create-loopback-interface) to enable the executor to access services from Docker containers. The loopback adapter automatically becomes the host for the Duo Workflow Service.
   - If you are using Colima, configure it to use your loopback IP address:

       ```shell
       colima stop
       colima start --network-address=true --dns-host gdk.test=<LOOPBACK_IP_ADDRESS>
       ```

1. Create a project in a group with experimental features and GitLab Duo turned on:
   1. Create or update a group by running the following [Rake task](https://docs.gitlab.com/ee/development/ai_features/#option-a-in-saas-gitlabcom-mode) in your GitLab repository directory:

      ```shell
      GITLAB_SIMULATE_SAAS=1 bundle exec 'rake gitlab:duo:setup[test-group-name]'
      ```

   1. Create a project in the group.

## Configure Duo Workflow components

1. In the root of your `<gdk-dir>` enable `duo-workflow-service` and configure your GitLab instance to use this locally running instance:

   ```shell
   gdk config set duo_workflow.enabled true
   gdk reconfigure
   gdk restart duo-workflow-service rails
   ```

1. Configure VS Code to use Docker by following the instructions in the [Duo Workflow user documentation](https://docs.gitlab.com/ee/user/duo_workflow/index.html#install-docker-and-set-the-socket-file-path).
1. Add a project to your VS Code workspace. 
   - The project should be in a group with experimental features and GitLab Duo turned on, as described in the Prerequisites section above.
   - Make sure it is the only repository open in the Source Control panel.

You can now [use Duo Workflow in VS Code](https://docs.gitlab.com/ee/user/duo_workflow/index.html#use-gitlab-duo-workflow-in-vs-code).

## Optional: Run a different branch of Duo Workflow Service

The
[`duo-workflow-service` repository](https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-service)
is cloned into `<gdk-dir>/duo-workflow-service`.

To configure GDK to run a specific branch, use either the branch name or SHA:

```shell
gdk config set duo_workflow.service_version <branch-name-or-SHA>
gdk reconfigure
```

## Optional: Run a different branch of Duo Workflow Executor

The [`duo-workflow-executor` repository](https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-executor) is
cloned into `<gdk-dir>/duo-workflow-executor` and compiled every time you run
`gdk reconfigure`. The binary is placed in `<gdk-dir>/gitlab/public/assets` and
served up by your local GitLab instance.

To change the version used:

1. Edit the `<gdk-dir>/gitlab/DUO_WORKFLOW_EXECUTOR_VERSION` with a valid SHA in the repository.
1. Recompile the binary:

   ```shell
   gdk reconfigure
   ```

## Optional: Configure LLM Cache

LLMs are slow and expensive. If you are doing lots of repetitive development
with Duo Workflow you may wish to enable
[LLM caching](https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-service#llm-caching)
to speed up iteration and save money. To enable the cache:

```shell
gdk config set duo_workflow.llm_cache true
gdk reconfigure
gdk restart duo-workflow-service rails
```
