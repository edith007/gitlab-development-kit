# Evaluating AI Models with ELI5

The `gdk evaluate` command allows you to evaluate AI models using the ELI5 project.

## Setup

1. See our [documentation](https://gitlab.com/gitlab-org/ai-powered/eli5/-/tree/main/doc?ref_type=heads) for more information on setting up and using the ELI5 project.
1. Enable the management of the ELI5 project by running:
   - `gdk config set gitlab_eli5.enabled true`
   - `gdk update`

## Usage

1. Run `gdk evaluate chat`.
1. Enter the dataset name when prompted.

Following these steps ensures that you have the necessary background information before enabling and using the ELI5 project.
