# frozen_string_literal: true

RSpec.describe GDK::Command::Version do
  include ShelloutHelper

  describe '#run' do
    it 'returns GitLab Development Kit 0.2.12 (abc123)' do
      stub_const('GDK::VERSION', 'GitLab Development Kit 0.2.12')
      shellout_double = gdk_shellout_double(run: 'abc123')
      allow_gdk_shellout_command('git rev-parse --short HEAD', chdir: GDK.root).and_return(shellout_double)

      expect { subject.run }.to output("GitLab Development Kit 0.2.12 (abc123)\n").to_stdout
    end
  end
end
