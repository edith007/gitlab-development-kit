# frozen_string_literal: true

module ShelloutHelper
  def allow_gdk_shellout
    allow(GDK::Shellout).to receive(:new)
  end

  def allow_gdk_shellout_command(*args, **kwargs)
    allow_gdk_shellout.with(*args, **kwargs)
  end

  def expect_gdk_shellout
    expect(GDK::Shellout).to receive(:new)
  end

  def expect_gdk_shellout_command(*args, **kwargs)
    expect_gdk_shellout.with(*args, **kwargs)
  end

  def expect_no_gdk_shellout
    expect(GDK::Shellout).not_to receive(:new)
  end

  def gdk_shellout_double(**kwargs)
    instance_double(GDK::Shellout, **kwargs)
  end
end
