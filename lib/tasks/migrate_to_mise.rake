# frozen_string_literal: true

namespace :mise do
  desc 'Migrate from asdf to mise'
  task :migrate do
    AsdfToMise.new.execute
  end
end

class AsdfToMise
  def execute
    disable_asdf
    enable_mise
    install_git_hooks
    remove_bootstrap_cache
    re_bootstrap
    remove_asdf_from_shell_configs
    display_success_message
  end

  private

  def disable_asdf
    GDK::Output.info('Opting out of asdf...')
    GDK::Command::Config.new.run(['set', 'asdf.opt_out', 'true'])
  end

  def remove_asdf_from_shell_configs
    GDK::Output.info('Removing "source" commands for asdf from shell config files...')
    GDK::Output.puts

    shell_configs = {
      '~/.bashrc' => 'asdf.sh',
      '~/.zshrc' => 'asdf.sh',
      '~/.config/fish/config.fish' => 'asdf.fish',
      '~/.config/elvish/rc.elv' => 'asdf.elv',
      '~/.config/nushell/config.nu' => 'asdf.nu'
    }

    @asdf_sourced_shell_configs = []

    shell_configs.each do |path, asdf_script|
      full_path = File.expand_path(path)
      next unless File.file?(full_path)

      content = File.read(full_path)
      @asdf_sourced_shell_configs << full_path if content.match?(/source.*asdf/)

      original = content.dup
      content.gsub!(%r{\n# Added by GDK bootstrap\nsource .*?/\.asdf/#{Regexp.escape(asdf_script)}$\n}, '')

      next if content == original

      backup_path = "#{full_path}.#{Time.now.strftime('%Y%m%d%H%M%S')}.bak"
      FileUtils.cp(full_path, backup_path)
      File.write(full_path, content)
      GDK::Output.success("Cleaned #{full_path} (backup created at #{backup_path})")
    rescue StandardError => e
      GDK::Output.error("Failed to clean #{full_path}: #{e.message}")
    end
  end

  def enable_mise
    GDK::Output.info('Enabling mise...')
    GDK::Command::Config.new.run(['set', 'mise.enabled', 'true'])
  end

  def install_git_hooks
    GDK::Output.info('Installing Git hooks...')
    run_command('lefthook install', 'lefthook install failed!')
  end

  def remove_bootstrap_cache
    GDK::Output.info('Removing cached bootstrap files...')
    cache_path = File.join(GDK.config.gdk_root, '.cache')
    %w[.gdk_bootstrapped .gdk_platform_setup].each do |file|
      FileUtils.rm_f(File.join(cache_path, file))
    end
  end

  def re_bootstrap
    GDK::Output.info('Running `bin/gdk-shell support/bootstrap` to install mise and dependencies...')
    run_command('bin/gdk-shell support/bootstrap', 'bin/gdk-shell support/bootstrap failed!')
  end

  def display_success_message
    GDK::Output.success('Migration from asdf to mise is almost complete!')
    GDK::Output.puts
    GDK::Output.notice('Next steps:')
    GDK::Output.puts

    step = 1

    return if @asdf_sourced_shell_configs.empty?

    unless @asdf_sourced_shell_configs.empty?
      GDK::Output.notice(<<~MESSAGE)
        #{step}. Found asdf configuration in:
           - #{@asdf_sourced_shell_configs.join("\n   - ")}
           If you only use asdf for GDK: You can remove the asdf source lines from these files
           If you use asdf for other projects: Keep these lines but this might conflict with GDK using mise to manage versions
      MESSAGE
      GDK::Output.puts
      step += 1
    end

    GDK::Output.notice("#{step}. If you're using a shell other than Bash or Zsh, please follow https://mise.jdx.dev/getting-started.html#_2-activate-mise to activate mise.")
    GDK::Output.puts

    GDK::Output.notice("#{step + 1}. Please restart your terminal. Afterward, run this command:")
    GDK::Output.puts('   gdk reconfigure && gdk update')
  end

  def run_command(command, error_message)
    Bundler.with_unbundled_env do
      sh = GDK::Shellout.new(command.split, chdir: GDK.config.gdk_root).execute
      raise StandardError, error_message unless sh.success?
    end
  end
end
