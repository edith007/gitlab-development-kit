# frozen_string_literal: true

require_relative '../gdk/task_helpers'

desc 'Run GitLab migrations'
task 'gitlab-db-migrate' do
  puts

  raise 'Failed to start services for database schema migration.' unless GDK::Command::Start.new.run(['rails-migration-dependencies'])

  raise 'Database schema migration failed.' unless GDK::TaskHelpers::RailsMigration.new.migrate
end
