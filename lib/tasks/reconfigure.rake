# frozen_string_literal: true

desc 'Reconfigure your GDK'
spinner_task reconfigure:  %w[
  reconfigure:make:Procfile
  reconfigure:make:postgresql
  reconfigure:subprojects
  reconfigure:make:gdk-reconfigure-task
].freeze

namespace :reconfigure do
  Support::Rake::Reconfigure.make_tasks.each do |make_task|
    desc "Run `make #{make_task.target}`"
    task "make:#{make_task.target}" do |t|
      t.skip! if make_task.skip?

      success = GDK.make(make_task.target).success?
      raise "make #{make_task.target} failed" unless success
    end
  end

  subprojects = %w[
    jaeger-setup
    openssh-setup
    nginx-setup
    registry-setup
    elasticsearch-setup
    gitlab-runner-setup
    runner-setup
    geo-config
    gitlab-topology-service-setup
    gitlab-http-router-setup
    gitlab-docs-setup
    docs-gitlab-com-setup
    gitlab-observability-backend-setup
    gitlab-elasticsearch-indexer-setup
    gitlab-k8s-agent-setup
    gitlab-pages-setup
    gitlab-ui-setup
    gitlab-zoekt-indexer-setup
    grafana-setup
    object-storage-setup
    openldap-setup
    pgvector-setup
    prom-setup
    snowplow-micro-setup
    zoekt-setup
    duo-workflow-service-setup
    duo-workflow-executor-setup
    postgresql-replica-setup
    postgresql-replica-2-setup
    openbao-setup
    siphon-setup
    nats-setup
  ].map { |task| "make:#{task}" }

  desc nil
  multitask subprojects: subprojects
end
