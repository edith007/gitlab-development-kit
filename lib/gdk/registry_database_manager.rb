# frozen_string_literal: true

module GDK
  class RegistryDatabaseManager
    def initialize(config)
      @config = config
    end

    def reset_registry_database
      stop_runit_services
      sleep(2)
      start_postgresql_service
      sleep(2)
      drop_database('registry_dev')
      recreate_database('registry_dev')
      migrate_database
    end

    def import_registry_data
      shellout(gdk_root.join('support', 'import-registry').to_s)
    end

    private

    attr_reader :config

    def gdk_root
      config.gdk_root
    end

    def start_postgresql_service
      GDK::Command::Start.new.run(['postgresql', '--quiet'])
    end

    def stop_runit_services
      GDK::Command::Stop.new.run([])
    end

    def drop_database(database_name)
      shellout(psql_cmd("drop database #{database_name}"))
    end

    def recreate_database(database_name)
      shellout(psql_cmd("create database #{database_name}"))
    end

    def migrate_database
      shellout(gdk_root.join('support', 'migrate-registry').to_s)
    end

    def psql_cmd(command)
      GDK::Postgresql.new(config).psql_cmd(['-c'] + [command])
    end

    def shellout(command)
      sh = Shellout.new(command, chdir: gdk_root)
      sh.stream
      sh.success?
    end
  end
end
