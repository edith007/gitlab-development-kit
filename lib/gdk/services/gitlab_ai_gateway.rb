# frozen_string_literal: true

module GDK
  module Services
    class GitlabAiGateway < Base
      def name
        'gitlab-ai-gateway'
      end

      def command
        config.gitlab_ai_gateway.__service_command
      end

      def enabled?
        config.gitlab_ai_gateway.enabled?
      end

      def ready_message
        "GitLab AI Gateway is available at #{config.gitlab_ai_gateway.__listen}/docs."
      end
    end
  end
end
