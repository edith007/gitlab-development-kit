# frozen_string_literal: true

require 'pathname'

module GDK
  module Services
    class SiphonClickhouseConsumer < Base
      def name
        'siphon-clickhouse-consumer'
      end

      def command
        'siphon/cmd/clickhouse_consumer/clickhouse_consumer --configFile siphon/consumer.yml'
      end

      def ready_message
        "Siphon ClickHouse consumer is listening for data from configured tables: #{config.siphon.tables.join(', ')}"
      end

      def validate!
        return unless config.siphon.enabled?
        return if config.clickhouse.enabled? && config.nats.enabled?

        raise GDK::ConfigSettings::UnsupportedConfiguration, <<~MSG.strip
          Running Siphon without ClickHouse and NATS is not possible.
          Enable ClickHouse and NATS in your GDK or disable Siphon to continue.
        MSG
      end

      def enabled?
        return false unless config.clickhouse.enabled? && config.nats.enabled?

        config.siphon.enabled?
      end
    end
  end
end
