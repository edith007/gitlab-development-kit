# frozen_string_literal: true

module GDK
  module Services
    class GitlabDocs < Base
      def name
        'gitlab-docs'
      end

      def enabled?
        config.gitlab_docs.enabled?
      end

      def command
        %(support/gitlab-docs/start-nanoc)
      end
    end
  end
end
