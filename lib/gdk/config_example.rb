# frozen_string_literal: true

require_relative 'config'

module GDK
  # Config subclass to generate gdk.example.yml
  class ConfigExample < Config
    # Module that stubs reading from the environment
    module Stubbed
      def find_executable!(_bin)
        nil
      end

      def rand(max = 0)
        return max.first if max.is_a?(Range)

        0
      end

      def settings_klass
        ::GDK::ConfigExample::Settings
      end
    end

    # Environment stubbed GDK::ConfigSettings subclass
    class Settings < ::GDK::ConfigSettings
      prepend Stubbed
    end

    prepend Stubbed

    GDK_ROOT = '/home/git/gdk'

    # Avoid messing up the superclass (i.e. `GDK::Config`)
    @attributes = superclass.attributes.dup

    def initialize
      # Override some settings which would otherwise be auto-detected
      yaml = {
        'username' => 'git',
        'git_repositories' => [],
        'restrict_cpu_count' => -1,
        'postgresql' => {
          'bin_dir' => '/usr/local/bin'
        }
      }

      super(yaml: yaml)
    end
  end
end
